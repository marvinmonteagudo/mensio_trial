<?php

namespace App\Http\Controllers;

use App\Invoice;
Use App\Customer;
Use App\PaymentType;

use App\Product;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = [
            'page_name' => 'invoice',
            'page_title' => 'Manage Invoices',
        ];
        $allInvoices = Invoice::all();

        foreach($allInvoices as $key=>$invoice) {
	        $payment_type_name = PaymentType::select( 'payment_type' )
	                                        ->where( 'id', '=', $invoice['payment_type_id'] )
	                                        ->pluck('payment_type');
	        $allInvoices[ $key ]['payment_type_name'] = $payment_type_name[0];
	        $customer_name = Customer::select( 'surname', 'firstname' )
	                                        ->where( 'id', '=', $invoice['customer_id'] )
	                                        ->get();
	        $allInvoices[ $key ]['customer_name'] = $customer_name[0]['surname'] . ", " . $customer_name[0]['firstname'];
        }
        //dd($allInvoices);
        return view ('invoice-main', compact('page', 'allInvoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$page = [
		    'page_name' => 'invoice',
		    'page_title' => 'Manage Invoices',
	    ];

    	$allProducts = Product::all();
    	$allCustomers = Customer::all();
        return view('invoice-create', compact('page', 'allProducts', 'allCustomers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }
}
