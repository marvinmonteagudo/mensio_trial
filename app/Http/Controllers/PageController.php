<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //
	public function dashboard() {
		$page = [
			'page_name' =>'dashboard',
			'page_title' => 'Dashboard',
		];
		return view('dashboard', compact('page'));
	}

}
