<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */


    public function run()
    {
        // $this->call(UsersTableSeeder::class);
	    $faker = Faker::create();
	    foreach(range(1, 10) as $idx) {
	    	DB::table('products')->insert ([
				'name' =>  $faker->words(2, true),
			    'description' => $faker->paragraph,
			    'price' => $faker->randomFloat(null, 0 ,50.00),
				'tax' => $faker->randomFloat(null, 0 ,5.00),
		    ]);
	    	DB::table('customers')->insert([
				'surname' => $faker->lastName,
			    'firstname' => $faker->firstName,
			    'email' => $faker->email,
			    'address' => $faker->address,
		    ]);
	    }

	    DB::table('payment_types')->insert ([
			'payment_type' => 'Cash',
		    'description' => $faker->paragraph,
	    ]);
	    DB::table('payment_types')->insert ([
		    'payment_type' => 'Check',
		    'description' => $faker->paragraph,
	    ]);
	    DB::table('payment_types')->insert ([
		    'payment_type' => 'Credit Card',
		    'description' => $faker->paragraph,
	    ]);


    }
}
