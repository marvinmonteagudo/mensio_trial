$(document).ready(function(){
    var theURL = $("#theURL").val();

    $("a.undercons").on("click", function(e){
        e.preventDefault();
        var notifpars = {
            icon: "pe-7s-config",
            text: "This area is currently <strong>Under Construction</strong>.<br>Please come back after a few days.",
            type: "warning"
        }
        doNotify(notifpars);
    });

    $("#btnAddInvoiceItem").on("click", function(e){
        e.preventDefault();
        var itmId = $("#frmAddInvoice #product_id").val();
        var itmProduct = $("#frmAddInvoice #product_id option:selected").text();
        var itmPrice = $("#frmAddInvoice #price").val();
        var itmTax = $("#frmAddInvoice #tax").val();

        $("#tblInvoiceItems").append($('<tr/>')
            .append($('<td/>').text(itmId))
            .append($('<td/>').text(itmProduct))
            .append($('<td/>').text(itmPrice))
            .append($('<td/>').text(itmTax))
            .append($('<td/>')
                .append($('<button/>')
                    .addClass('btn btn-fill btn-danger btn-xs deleteRow')
                    .html('<i class="fa fa-trash"></i> Remove')
                )
            )
        );
        $("#frmAddInvoice #product_id").val(0).select();
        $("#frmAddInvoice #price").val('');
        $("#frmAddInvoice #tax").val('');
        $("#btnAddInvoiceItem").attr("disabled", true);

    });

    $("#frmAddInvoice #product_id").on("change", function(e){
        e.preventDefault();
        var productId = $(this).val();

        if (productId > 0) {
            $("#btnAddInvoiceItem").attr("disabled", false);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            var request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: theURL + "/product/getdetail",
                method: 'post',
                data: {
                    id: productId,
                }
            });

            request.done( function(data) {
                if (data.status == "success") {
                    $("#frmAddInvoice #price").val(data.info.price)
                    $("#frmAddInvoice #tax").val(data.info.tax)
                }
            });
        } else {
            $("#frmAddInvoice #price").val('');
            $("#frmAddInvoice #tax").val('');
            $("#btnAddInvoiceItem").attr("disabled", true);
        }



    });

    $("#frmAddInvoice #customer_id").on("change", function(e){
        e.preventDefault();
        var now = new Date();
        now.setDate(now.getDate() + 15);
        $("#frmAddInvoice #date_issued").val($.datepicker.formatDate('yy-mm-dd', new Date()));
        $("#frmAddInvoice #date_due").val($.datepicker.formatDate('yy-mm-dd', now));
    });

    $("#tblInvoiceItems").on("click", ".deleteRow", function(e){
        e.preventDefault();
        $(this).closest('tr').remove();
    });


    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });
});


function doNotify(params = []) {
    var theIcon = params.icon ? params.icon : 'pe-7s-config';
    var theText = params.text ? params.text : "This is a sample notification.";
    var theType = params.type ? params.type : 'info';

    $.notify({
        icon: theIcon,
        message: theText

    },{
        type: theType,
        timer: 2000,
        delay: 1000,
    });
}