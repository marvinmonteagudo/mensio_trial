@extends('layouts.lightbootstrap.master')
@section('page-content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title" style="font-weight: bold">CREATE NEW INVOICE</h4>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-10 col-md-push-1">
                            <form id="frmAddInvoice">
                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <label>Customer Name</label>
                                        <select class="form-control" name="customer_id" id="customer_id">
                                            <option value="0">-- Select a customer</option>
                                            @foreach($allCustomers as $customer)
                                                <option value="{{$customer->id}}">{{$customer->surname}}, {{$customer->firstname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Date Issued</label>
                                        <input type="text" name="date_issued" id="date_issued" class="form-control datepicker">
                                    </div>
                                    <div class="col-md-3">
                                        <label>Due Date</label>
                                        <input type="text" name="date_due" id="date_due" class="form-control datepicker">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label>Note</label>
                                        <textarea name="note" id="note" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-push-1">
                                        <h4 style="font-weight: bold">INVOICE ITEMS</h4>
                                        <div class="col-md-5">
                                            <label>Product</label>
                                            <select class="form-control" name="product_id" id="product_id">
                                                <option value="0">-- Select a Product</option>
                                                @foreach($allProducts as $product)
                                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Price </label>
                                            <input type="text" name="price" id="price" class="form-control">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Tax</label>
                                            <input type="text" name="tax" id="tax" class="form-control">
                                        </div>
                                        <div class="col-md-1">
                                            <label style="display: block">&nbsp;</label>
                                            <button class="btn btn-fill btn-primary" id="btnAddInvoiceItem" disabled><i class="fa fa-plus"></i> ADD </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-8 col-md-push-2">
                                    <div class="table-responsive table-full-width table-striped">
                                        <table class="table table-hover" id="tblInvoiceItems">
                                            <thead>
                                            <th>ID</th>
                                            <th>Product</th>
                                            <th>Price</th>
                                            <th>Tax</th>
                                            <th>Actions</th>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="text-align:center">
                                        <button class="btn btn-fill btn-success" id="btnSubmitInvoice"><i class="fa fa-save"></i> Save Invoice</button>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
