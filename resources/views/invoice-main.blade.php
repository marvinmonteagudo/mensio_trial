@extends('layouts.lightbootstrap.master')
@section('page-content')
    <div class="row">
        @if (count($allInvoices) > 0)
            <div class="col-md-12">
                <div align="right" style="display: block; margin-bottom: 15px">
                    <a href="/invoice/create" class="btn btn-success btn-fill" id="btnAddInvoice"><i class="fa fa-plus"></i> Create New Invoice</a>
                </div>
                <div class="card">
                    <div class="header">
                        <h4 class="title">List of all Invoices</h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover">
                            <thead>
                                <th>Invoice #</th>
                                <th>Customer Name</th>
                                <th>Date Issued</th>
                                <th>Due Date</th>
                                <th>Payment Type</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                @foreach($allInvoices as $invoice)
                                    <tr>
                                        <td>{{ $invoice->invoice_num }}</td>
                                        <td>{{ $invoice->customer_name }}</td>
                                        <td>{{ date('F j, Y', strtotime($invoice->date_issued)) }}</td>
                                        <td>{{ date('F j, Y', strtotime($invoice->date_due)) }}</td>
                                        <td>{{ $invoice->payment_type_name }}</td>
                                        <td>
                                            <a href="" class="btn btn-xs btn-fill btn-primary" title="View Invoice Details"><i class="fa fa-eye"></i>View Details</a>
                                            <a href="" class="btn btn-xs btn-fill btn-primary" title="Edit Invoice Details"><i class="fa fa-pencil"></i>Edit Invoice</a>
                                            <a href="" class="btn btn-xs btn-fill btn-danger" title="Delete Invoice"><i class="fa fa-trash"></i>Delete Invoice</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-6 col-md-push-3">
                <div class="card">
                    <div class="header">
                        <div align="center">
                            <h4 class="title">There are currently no invoices saved.</h4>
                        </div>
                        <div align="center" style="display: block; margin: 15px 0">
                            <button class="btn btn-success btn-fill" add="btnAddInvoice"><i class="fa fa-plus"></i>Add your first invoice</button>
                        </div>
                    </div>
                    <div class="footer">
                        &nbsp;
                    </div>
                </div>
            </div>
        @endif
    </div>



@endsection
