@include('layouts.lightbootstrap.part.head')
<body>
<input type="hidden" id="theURL" value="{{ env('APP_URL') }}">
<div class="wrapper">
    <!-- Sidebar Area -->
    @include('layouts.lightbootstrap.part.sidebar')
    <!-- END Sidebar Area -->

    <!-- Main Panel Area -->
    <div class="main-panel">
        <!-- Top Navigation Bar -->
        @include('layouts.lightbootstrap.part.topbar')
        <!-- END Top Navigation Bar -->

        <div class="content">
            <div class="container-fluid">
                @yield('page-content')
            </div>
        </div>

        <!-- Footer Area -->
        @include('layouts.lightbootstrap.part.footer')
        <!-- END Footer Area -->
    </div>
    <!-- END Main Panel Area -->
</div>


</body>

<!-- Javascript Area -->
@include('layouts.lightbootstrap.part.jscripts')
<!-- END Javascript Area -->
</html>
