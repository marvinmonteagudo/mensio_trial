<footer class="footer">
    <div class="container-fluid">

        <p class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script> Marvin Monteagudo
        </p>
    </div>
</footer>
