<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Hearing Center Canada</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="_token" content="{{csrf_token()}}" />


    <!-- Bootstrap core CSS     -->
    <link href="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/css/demo.css" rel="stylesheet" />


    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>