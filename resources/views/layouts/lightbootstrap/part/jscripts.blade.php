<!--   Core JS Files   -->
<script src="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css">
<script src="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/js/demo.js"></script>

<!-- Mensio Trial Custom JS -->
<script src="{{ env('APP_URL') }}/js/mensiotrial.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        demo.initChartist();


    });
</script>
