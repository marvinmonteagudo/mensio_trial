<div class="sidebar" data-color="orange" data-image="{{ env('APP_URL') }}/themes/{{ env('CUST_PATH_THEME') }}/img/sidebar-5.jpg">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{ env('APP_URL') }}" class="simple-text">
                {{ env('CUST_BIZNAME') }}
            </a>
        </div>

        <ul class="nav">
            <li @if ($page['page_name'] == 'dashboard') class="active" @endif>
                <a href="/">
                    <i class="pe-7s-graph"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li @if ($page['page_name'] == 'invoice') class="active" @endif>
                <a href="/invoice">
                    <i class="pe-7s-news-paper"></i>
                    <p>Manage Invoices</p>
                </a>
            </li>
            <li>
                <a class="undercons">
                    <i class="pe-7s-users"></i>
                    <p>Manage Customers</p>
                </a>
            </li>
            <li>
                <a class="undercons">
                    <i class="pe-7s-credit"></i>
                    <p>Manage Payment Types</p>
                </a>
            </li>

            <li>
                <a href="icons.html">
                    <i class="pe-7s-cart"></i>
                    <p>Manage Products</p>
                </a>
            </li>
            <li>
                <a class="undercons">
                    <i class="pe-7s-config"></i>
                    <p>Settings</p>
                </a>
            </li>
        </ul>
    </div>
</div>