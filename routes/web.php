<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Use App\Http\Controllers\PageController;
Use App\Http\Controllers\InvoiceController;
Use App\Http\Controllers\InvoiceDetailController;
Use App\Http\Controllers\ProductController;

Route::get('/', 'PageController@dashboard');

//Invoice Management
Route::get('/invoice', 'InvoiceController@index');
Route::get('/invoice/create', 'InvoiceController@create');

Route::post('/product/getdetail', 'ProductController@getDetail');